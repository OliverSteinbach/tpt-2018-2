module.exports = function(number) {
    const n = +number;
    if (isNaN(n)) {
        throw new Error(`value '${number}' is not a number!`);
    }
    return n;
};
